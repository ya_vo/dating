Dating::Application.routes.draw do
  get "messages/index"
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index', as: :home

  get 'profiles' => 'profiles#index'
  patch 'profile' => 'profiles#update', as: :update_profile
  get 'profile/edit' => 'profiles#edit', as: :edit_profile
  get 'p/:username' => 'profiles#show', as: :profile
  post 'p/:username' => 'profiles#create_share', as: :shares

  get 'images/new' => 'images#new', as: :new_image
  get 'i/:username' => 'images#index', as: :images
  get 'i/:username/:id', to: 'images#show', as: :image
  post 'images/create' => 'images#create', as: :create_image
  delete 'images/destroy', to: 'images#destroy', as: :destroy_image
  put 'images/avatar' => 'images#avatar'

  post 'comments/create', to: 'comments#create', as: :create_comment

  get 'messages/:username', to: 'messages#index', as: :messages
  post 'messages', to: 'messages#create', as: :create_message


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
