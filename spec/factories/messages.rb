# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    author_id ""
    recipient_id ""
    read ""
    content "MyString"
    text "MyString"
  end
end
