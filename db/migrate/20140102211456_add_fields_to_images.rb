class AddFieldsToImages < ActiveRecord::Migration
  def change
    add_column :images, :user_id, :integer
    add_column :images, :filename, :string
  end
end
