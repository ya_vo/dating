class AddAuthorIdColumnToShares < ActiveRecord::Migration
  def change
    add_column :shares, :author_id, :integer
    add_index :shares, :author_id
  end

end
