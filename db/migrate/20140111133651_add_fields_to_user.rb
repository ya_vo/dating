class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :sex, :string
    add_column :users, :age, :integer
    add_column :users, :city_id, :integer
    add_column :users, :height, :integer
    add_column :users, :weight, :integer
    add_column :users, :eye_color, :string
    add_column :users, :family, :string
    add_column :users, :pets, :string
    add_column :users, :hobbies, :string
    add_column :users, :sports, :string
    add_column :users, :movies, :string
    add_column :users, :music, :string
    add_column :users, :books, :string
    add_column :users, :likes, :string
    add_column :users, :dislikes, :string
  end
end
