class AddAvatarToImages < ActiveRecord::Migration
  def change
    add_column :images, :avatar, :boolean
    add_index :images, :avatar
  end
end
