class ReplaceAgeWithBirthdayInUsers < ActiveRecord::Migration
  def change
    add_column :users, :birthday, :date
    remove_column :users, :age
    add_index :users, :birthday
  end
end
