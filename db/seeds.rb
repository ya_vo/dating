# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require 'csv'


csv_file_path = 'db/cities.csv'
City.delete_all
CSV.foreach(csv_file_path) do |row|
  City.create!({
    :name => row[1]
    })
  puts "#{row[1]} added"
end
