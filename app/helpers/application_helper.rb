module ApplicationHelper
  def image_thumb(image, html = '')
    if image.nil?
      return "<img src='#{asset_url('images/anonymous-user.png')}' #{html}>".html_safe
    end
    "<img src='#{image.filename.thumb.url}' alt='#{image.filename.thumb}' #{html} >".html_safe
  end

  def image_medium(image, html = '')
    if image.nil?
      return "<img src='#{asset_url('images/anonymous-user.png')}' #{html}>".html_safe
    end
    "<img src='#{image.filename.medium.url}' alt='#{image.filename.medium}' #{html} >".html_safe
  end

  def image_full(image, html = '')
    if image.nil?
      return "<img src='#{asset_url('images/anonymous-user.png')}' #{html}>".html_safe
    end
    "<img src='#{image.filename.url}' alt='#{image.filename}' #{html} >".html_safe
  end



end
