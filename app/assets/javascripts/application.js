// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//For template
//= require components/library/jquery/jquery.min
//= require components/library/jquery/jquery-migrate.min
//= require components/library/modernizr/modernizr
//= require components/plugins/less-js/less.min
//= require components/modules/admin/charts/flot/assets/lib/excanvas
//= require components/plugins/browser/ie/ie.prototype.polyfill
//= require components/library/jquery-ui/js/jquery-ui.min
//= require components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min

//= require jquery_ujs
//= require turbolinks

$.ajaxSetup({
    'beforeSend': function(xhr) {
        xhr.setRequestHeader("Accept", "text/javascript")
    }
});

