/* Remove Envato Frame */
if (window.location != window.parent.location)
	top.location.href = document.location.href;

var randNum,
	equalHeight,
	genSparklines,
	beautify,
	mt_rand;

(function($, window)
{

	window.onunload = function(){};

	$.expr[':'].scrollable = function( elem )
    {
      var scrollable = false,
          props = [ '', '-x', '-y' ],
          re = /^(?:auto|scroll)$/i,
          elem = $(elem);

      $.each( props, function(i,v){
        return !( scrollable = scrollable || re.test( elem.css( 'overflow' + v ) ) );
      });

      return scrollable;
    };

	if (!Modernizr.touch && $('[href="#template-options"][data-auto-open]').length)
		$('#template-options').collapse('show');

	// generate a random number
	window.randNum = function()
	{
		return (Math.floor( Math.random()* (1+40-20) ) ) + 20;
	}

	window.equalHeight = function(boxes, substract)
	{
		if (typeof substract == 'undefined')
			var substract = 0;

		boxes.height('auto');
		if (parseInt($(window).width()) <= 400)
			return;

		var maxHeight = Math.max.apply( Math, boxes.map(function(){ return $(this).height() - substract; }).get());
		boxes.height(maxHeight);
	}

	function parse_url (str, component) {
		var query, key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port',
		                  'relative', 'path', 'directory', 'file', 'query', 'fragment'],
		                  ini = (this.php_js && this.php_js.ini) || {},
		                  mode = (ini['phpjs.parse_url.mode'] &&
		                		  ini['phpjs.parse_url.mode'].local_value) || 'php',
		                		  parser = {
		                	  php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		                	  strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		                	  loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // Added one optional slash to post-scheme to catch file:/// (should restrict this)
		                  };

		                  var m = parser[mode].exec(str),
		                  uri = {},
		                  i = 14;
		                  while (i--) {
		                	  if (m[i]) {
		                		  uri[key[i]] = m[i];
		                	  }
		                  }

		                  if (component) {
		                	  return uri[component.replace('PHP_URL_', '').toLowerCase()];
		                  }
		                  if (mode !== 'php') {
		                	  var name = (ini['phpjs.parse_url.queryKey'] &&
		                			  ini['phpjs.parse_url.queryKey'].local_value) || 'queryKey';
		                	  parser = /(?:^|&)([^&=]*)=?([^&]*)/g;
		                	  uri[name] = {};
		                	  query = uri[key[12]] || '';
		                	  query.replace(parser, function ($0, $1, $2) {
		                		  if ($1) {uri[name][$1] = $2;}
		                	  });
		                  }
		                  delete uri.source;
		                  return uri;
	}

	function PDFTarget(target)
	{
		var doc = $('html').clone();
		var target = $(target).clone();
		var form = $('#PDFTargetForm');
		if (!form.length) {
			$('<form id="PDFTargetForm"></form>').appendTo('body');
			form = $('#PDFTargetForm');
		}

		form.attr('action', rootPath + 'admin/php/pdf.php');
		form.attr('method', 'POST');
		$('<input type="hidden" name="target" value="" />').appendTo(form);

		target.find('.hidden-print').remove();
		doc.find('body').html(target);
		var html = doc.html();

		form.find('input').val(html);
		form.submit();
	}

	// save to PDF
	$('[data-toggle*="pdf"]').on('click', function(e){
		e.preventDefault();
		PDFTarget($(this).attr('data-target'));
	});

	window.beautify = function (source)
	{
		var output,
			opts = {};

	 	opts.preserve_newlines = false;
		output = html_beautify(source, opts);
	    return output;
	}

	// generate a random number within a range (PHP's mt_rand JavaScript implementation)
	window.mt_rand = function (min, max)
	{
		var argc = arguments.length;
		if (argc === 0) {
			min = 0;
			max = 2147483647;
		}
		else if (argc === 1) {
			throw new Error('Warning: mt_rand() expects exactly 2 parameters, 1 given');
		}
		else {
			min = parseInt(min, 10);
			max = parseInt(max, 10);
		}
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// scroll to element animation
	function scrollTo(id)
	{
		if ($(id).length)
			$('html,body').animate({scrollTop: $(id).offset().top},'slow');
	}

	var hideDropdownTimeout = null;

	$('#menu .dropdown')
	.on('mousemove', function(e)
	{
		e.stopPropagation();
		clearTimeout(hideDropdownTimeout);

		var t = $(this),
			m = t.find('> .dropdown-menu').clone();

		$('body')
		.find('> .dropdown-menu')
		.remove()
		.end()
		.append(m);

		m.show().css({
			left: 100,
			top: t.offset().top,
			width: 160
		});

	})
	.on('mouseleave', function()
	{
		hideDropdownTimeout = setTimeout(function(){
			$('body > .dropdown-menu').remove();
		}, 100);
	});

	$('body')
	.on('mouseenter', ' > .dropdown-menu', function()
	{
		clearTimeout(hideDropdownTimeout);
	})
	.on('mouseleave', ' > .dropdown-menu', function()
	{
		$(this).remove();
	});

	if (!Modernizr.touch && $('#menu').is(':visible'))
		$('.container-fluid').removeClass('menu-hidden');

	if (Modernizr.touch)
		$('#menu').removeClass('hidden-xs');

	// handle menu toggle button action
	function toggleMenuHidden()
	{
		if ($('.container-fluid.menu-right-visible').length)
			$('.container-fluid').removeClass('menu-right-visible');

		$('.container-fluid').toggleClass('menu-hidden menu-left-visible');
		$('#menu').removeClass('hidden-xs');

		resizeNiceScroll();
	}

	function resizeNiceScroll()
	{
		setTimeout(function(){
			$('.hasNiceScroll, #menu_kis, #menu').getNiceScroll().show().resize();
		}, 100);
	}

	// main menu visibility toggle
	$('.navbar.main .btn-navbar, #menu .btn-navbar').click(function()
	{
		toggleMenuHidden();
	});

	$('.btn-navbar-right').click(function(){
		$('.container-fluid').toggleClass('menu-right-visible');

		if ($('.container-fluid.menu-left-visible').length)
			$('.container-fluid').addClass('menu-hidden').removeClass('menu-left-visible');

		resizeNiceScroll();
	});

	if ($('.btn-navbar-right').length && !$('#menu_kis').length)
		$('.btn-navbar-right').parent().remove();

    $('#content .modal').appendTo('body');

	// tooltips
	$('body').tooltip({ selector: '[data-toggle="tooltip"]' });

	// popovers
	$('[data-toggle="popover"]').popover();

	// print
	$('[data-toggle="print"]').click(function(e)
	{
		e.preventDefault();
		window.print();
	});

	// carousels
	$('.carousel').carousel();

	// Google Code Prettify
	if ($('.prettyprint').length && typeof prettyPrint != 'undefined')
		prettyPrint();

	// show/hide toggle buttons
	$('[data-toggle="hide"]').click(function()
	{
		if ($(this).is('.bootboxTarget'))
			bootbox.alert($($(this).attr('data-target')).html());
		else {
			$($(this).attr('data-target')).toggleClass('hide');
			if ($(this).is('.scrollTarget') && !$($(this).attr('data-target')).is('.hide'))
				scrollTo($(this).attr('data-target'));
		}
	});

	$('ul.collapse')
	.on('show.bs.collapse', function(e)
	{
		e.stopPropagation();
		$(this).closest('li').addClass('active');
	})
	.on('hidden.bs.collapse', function(e)
	{
		e.stopPropagation();
		$(this).closest('li').removeClass('active');
	});

	window.enableContentNiceScroll = function(hide)
	{
		if ($('html').is('.ie') || Modernizr.touch)
			return;

		if (typeof hide == 'undefined')
			var hide = true;

		$('#content .col-app, .col-separator')
		.filter(':scrollable')
		.not('.col-unscrollable')
		.filter(function(){
			return !$(this).find('> .col-table').length;
		})
		.addClass('hasNiceScroll')
		.each(function()
		{
			$(this).niceScroll({
				horizrailenabled: false,
				zindex: 2,
				cursorborder: "none",
				cursorborderradius: "0",
				cursorcolor: primaryColor
			});

			if (hide == true)
				$(this).getNiceScroll().hide();
			else
				$(this).getNiceScroll().resize().show();
		});
	}

	window.disableContentNiceScroll = function()
	{
		$('#content .hasNiceScroll').getNiceScroll().remove();
	}

	enableContentNiceScroll();

	if ($('html').is('.ie'))
		$('html').removeClass('app');

	$('#menu > div')
	.add('#menu_kis > div')
	.addClass('hasNiceScroll')
	.niceScroll({
		horizrailenabled: false,
		zindex: 2,
		cursorborder: "none",
		cursorborderradius: "0",
		cursorcolor: primaryColor
	}).hide();

	window.closeDiscover = function()
	{
		var discover = $('#discover'),
			target = discover.find('> div');

		if (!target.length)
			return;

		target.attr('id', target.data('id'));
		target.attr('class', target.data('class'));
		target.insertAfter('#sidebar-discover-wrapper > ul > li > a[href="#' + target.attr('id') + '"]');
	}

	$('#sidebar-discover-wrapper > ul > li > a').on('click', function(e)
	{
		closeDiscover();

		if ($(this).is('[data-toggle="sidebar-discover"]'))
			e.preventDefault();

		if ($('#sidebar-discover-wrapper.open').length)
		{
			e.preventDefault();
			e.stopPropagation();
		}

		if ($('#sidebar-discover-wrapper.open').length)
		{
			$('#sidebar-discover-wrapper, [data-toggle="sidebar-discover"]').removeClass('open hover-closed');
			closeDiscover();
			return;
		}

		var that = $(this);

		$('[data-toggle="sidebar-discover"]').removeClass('open');
		that.addClass('open');

		var wrapper = $('#sidebar-discover-wrapper'),
			main = wrapper.find('> ul'),
			discover = wrapper.find('> #discover'),
			target = $(that.attr('href'));

		target.data('id', target.attr('id'));
		target.data('class', target.attr('class'));
		target.removeAttr('class id');

		if (!discover.length)
		{
			discover = $('<div/>').attr('id', 'discover');
			wrapper.append(discover);
		}

		discover.html(target);
		wrapper.addClass('open');
	})
	.on('mouseenter', function()
	{
		if ($(this).is('[data-toggle="sidebar-discover"].open'))
			return;

		if ($('#sidebar-discover-wrapper.open').length)
			$('#sidebar-discover-wrapper').addClass('hover-closed').removeClass('open');
	})
	.on('mouseleave', function()
	{
		$('#sidebar-discover-wrapper.hover-closed').removeClass('hover-closed').addClass('open');
	});

	$('body')
	.on('mouseenter', '[data-toggle="dropdown"].dropdown-hover', function()
	{
		if (!$(this).parent('.dropdown').is('.open'))
			$(this).click();
	});

	$('.navbar.main')
	.add('#menu-top')
	.on('mouseleave', function(){
		$(this).find('.dropdown.open').find('> [data-toggle="dropdown"]').click();
	});

	$('[data-height]').each(function(){
		$(this).height($(this).data('height'));
	});

	window.enableNavbarMenusHover = function(){
		$('.navbar.main [data-toggle="dropdown"]')
		.add('#menu-top [data-toggle="dropdown"]')
		.addClass('dropdown-hover');
	}

	window.disableNavbarMenusHover = function(){
		$('.navbar.main [data-toggle="dropdown"]')
		.add('#menu-top [data-toggle="dropdown"]')
		.removeClass('dropdown-hover');
	}

	window.enableResponsiveNavbarSubmenus = function(){
		$('.navbar .dropdown-submenu > a').on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			$(this).parent().toggleClass('open');
		});
	}

	window.disableResponsiveNavbarSubmenus = function(){
		$('.navbar .dropdown-submenu > a')
		.off('click')
		.parent()
		.removeClass('open');
	}

	$(window).setBreakpoints({
		distinct: false,
		breakpoints: [
			768,
			992
		]
	});

	$(window).bind('exitBreakpoint768',function() {
		$('.container-fluid').addClass('menu-hidden');
		disableNavbarMenusHover();
		enableResponsiveNavbarSubmenus();
	});

	$(window).bind('enterBreakpoint768',function() {
		$('.container-fluid').removeClass('menu-hidden');
		enableNavbarMenusHover();
		disableResponsiveNavbarSubmenus();
	});

	$(window).bind('exitBreakpoint992',function() {
		disableContentNiceScroll();
	});

	$(window).bind('enterBreakpoint992',function() {
		enableContentNiceScroll(false);
	});

	$(window).on('load', function()
	{
		if ($(window).width() < 992)
			$('.hasNiceScroll').getNiceScroll().stop();

		if ($(window).width() < 768)
			enableResponsiveNavbarSubmenus();
		else
			enableNavbarMenusHover();

		if (typeof animations == 'undefined')
			$('.hasNiceScroll, #menu_kis, #menu').getNiceScroll().show().resize();

		if (typeof Holder != 'undefined')
		{
			Holder.add_theme("dark", {background:"#424242", foreground:"#aaa", size:9}).run();
			Holder.add_theme("white", {background:"#fff", foreground:"#c9c9c9", size:9}).run();
		}
	});

})(jQuery, window);
