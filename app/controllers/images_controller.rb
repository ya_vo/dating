class ImagesController < ApplicationController

  def new
    @image = Image.new
    @user = current_user
  end

  def create
    @image = current_user.images.create(image_params)
    redirect_to image_path(username: @image.user.username, id: @image.id)
  end


  def image_params
    params.require(:image).permit(:filename)
  end

  def index
    @user = User.find_by_username(params[:username])
    @images = @user.images.all
  end

  def show
    @user = User.find_by_username(params[:username])
    @image = Image.find(params[:id])
  end

  def destroy
    Image.find(params[:id]).delete
    redirect_to images_path :username => current_user.username
  end

  def avatar
    if !current_user.avatar.nil?
      current_user.avatar.update_attribute(:avatar, false)
    end
    Image.find(params[:id]).update_attribute :avatar, true
    redirect_to images_path :username => current_user.username
  end
end
