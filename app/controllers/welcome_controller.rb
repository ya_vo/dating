class WelcomeController < ApplicationController
  def index
    if user_signed_in?
      render :template =>  'dashboard/index'
    end
  end
end
