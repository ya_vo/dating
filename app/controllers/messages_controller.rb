class MessagesController < ApplicationController
  def index

    if !user_signed_in?
      redirect_to :root
    end

    @conversations = current_user.conversations
    @receiver = User.find_by_username(params[:username])
    set_conversation(@receiver.id)
  end

    #responds to AJAX post request
    def create
      if (user_signed_in?)
        message = current_user.messages.new(message_params)
        message.receiver = User.find(params[:message][:receiver_id])
        set_conversation(message.receiver.id)
        message.sender = current_user
        message.conversation = @conversation
        if message.save
          render :partial => 'messages/message.html', :locals =>{:message => message}
        end
      end
    end

    def message_params
      params.require(:message).permit(:content, :receiver_id)
    end

    private

    def set_conversation(receiver_id)
      @conversation = Conversation.where(
        '(sender_id = ? AND receiver_id = ?) OR (receiver_id =? AND sender_id = ?)',
        receiver_id, current_user.id, receiver_id, current_user.id)
      .order(id: :desc).first

      if @conversation.nil?
        @conversation = Conversation.create(sender_id: current_user.id, receiver_id: receiver_id)
      end
    end
  end
