class CommentsController < ApplicationController
  
  
  #responds to AJAX post request
  def create
    if (user_signed_in?)
      comment = current_user.comments.new(comment_params)
      if comment.save
        render :partial => 'profiles/tabs/share_comment.html', :locals =>{:comment => comment} 
      end       
    end    
  end
  
  def comment_params
    params.require(:comment).permit(:content,:commentable_type,:commentable_id)
  end
end