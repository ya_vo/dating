class ProfilesController < ApplicationController
  def index
    results_per_page = 10
    if params[:search]

      search_params = {:birthday => (params[:search][:age_max].to_i+1).years.ago.to_date..params[:search][:age_min].to_i.years.ago.to_date}

      if !params[:search][:city_id].empty?
        search_params[:city_id] = params[:search][:city_id]
      end
      if !params[:search][:sex].empty?
        search_params[:sex] = params[:search][:sex]
      end

      @profiles = User.where(search_params).paginate(page: params[:page], per_page: results_per_page)
    else
      @profiles = User.all.paginate(page: params[:page], per_page: results_per_page)
    end
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(user_params)
      redirect_to edit_profile_path, flash: { success: t('Successfully updated.')}
    else
      render action: 'edit'
    end
  end


  def show
    @user = User.find_by_username(params[:username]) || not_found
    @share ||= Share.new
    @shares = @user.shares_on_profile.all(:order => "id DESC")
    render 'show'
  end

  def create_share
    @share = current_user.shares_authored.new(share_params)
    @share.user_id = params[:share][:user_id]
    saved = @share.save
    if saved == true
      flash[:notice] = 'message'
      @share = Share.new
    end

    if saved == false
      show
      return
    else
      redirect_to  :action => 'show', :username =>params[:share][:username]
    end
  end

  private
  def share_params
    params.require(:share).permit(:content)
  end

  def user_params
    params.require(:user).permit( :birthday, :city_id, :sex, :height, :weight, :pets,
     :hobbies, :sports, :movies, :music, :books, :likes, :dislikes, :eye_color  )
  end

end
