class Conversation < ActiveRecord::Base
  belongs_to :sender, :class_name => "User"
  belongs_to :receiver, :class_name => "User"
  has_many :messages, :dependent => :destroy

  def other_user(current_user)
    if sender.id == current_user.id
      receiver
    else
      sender
    end
  end
end
