class Image < ActiveRecord::Base
  belongs_to :user
  mount_uploader :filename, ImageUploader


  def next
    image = Image.where("images.id > ? AND images.user_id = ?", self.id, self.user_id)
    .order("images.id ASC").first

    if image.nil?
      image = Image.where('images.user_id = ?', self.user_id).first
    else
      image
    end
  end

  def previous
    image = Image.where("images.id < ? AND images.user_id = ?", self.id, self.user.id)
    .order("images.id DESC").first

    if image.nil?
      image = Image.where('images.user_id = ?', self.user_id).last
    else
      image
    end
  end
end
