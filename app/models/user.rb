class User < ActiveRecord::Base
  extend Enumerize
  extend ActiveModel::Naming #for i18n

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,    :recoverable,
  :rememberable, :trackable, :validatable
  validates :username,
  presence: true,
  length: { minimum: 3 },
  uniqueness: true
  has_many :images
  has_many :shares_authored, :class_name => 'Share', :foreign_key => 'author_id'
  has_many :shares_on_profile, :class_name => 'Share', :foreign_key => 'user_id'
  has_many :comments, :class_name => 'Comment', :foreign_key => 'author_id'
  has_one  :avatar, :class_name => 'Image', :foreign_key => 'user_id',
  :conditions => { :avatar => true}
  belongs_to :city
  enumerize :sex, in: [:male, :female]
  enumerize :eye_color, in: [:amber, :blue, :brown, :gray, :green, :hazel]




  def age
    dob = birthday
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  def opposite_sex
    if sex == :male
      'female'
    else
      'male'
    end

  end



  def conversations
    Conversation.where('sender_id = ? OR receiver_id = ?', id, id)
  end

  def messages
    Message.where('sender_id = ? OR receiver_id = ?', id, id)
  end


end
