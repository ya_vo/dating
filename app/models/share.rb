class Share < ActiveRecord::Base
  validates :content,
    presence: true  
  belongs_to :author, :class_name => 'User'
  belongs_to :user, :class_name => 'User'
  has_many :comments, as: :commentable
end

