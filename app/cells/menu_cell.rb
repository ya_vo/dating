class MenuCell < Cell::Rails
  include Devise::Controllers::Helpers
  helper_method :user_signed_in?
  helper_method :current_user

  def show
    render
  end

end
